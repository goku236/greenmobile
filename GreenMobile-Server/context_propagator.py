"""
Context_propagator
===============

Server functionality. Saves the information regarding the clients
and processes the requests.

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

 """


import aggregator
from context import Context


__author__ = "Pedro.Alves, Filipe Barroso"

class Context_propagator:

    def __init__(self, network_transport,
                 aggregability,
                 aggregation_functions=dict()):
        """
        Initates instance of ContextPropagator
        
        :param: network_transport: must have a method called send(context, person)
        :param: aggregability: function that receives a context and a person and returns how 
            many contexts like those can be aggregated before being sent to this person
        :param aggregation_functions: functions that try to aggregate a previous context to a 
            new context into one and receive a maximum aggregation level
        """

        assert isinstance(aggregability or [], list)

        self.network_transport = network_transport
        self.participants = {}
        self.aggregator = aggregator.Aggregator(aggregability, aggregation_functions)
        
    def register_participant(self, participant, data=dict()):
        """
        Registers a participant with (optionally) some data associated (e.g., endpoint)
        From this moment on, this participant will start receiving messages
        
        :param participant: name of the client
        :param data: endpoint information
        """
        self.participants[participant] = data

    def unregister_participant(self, participant):
        """ 
        Unregisters a participant. All pending messages will be propagated but new messages won't be
        
        :param: paricipant: name of the client
        """
        
        context_to_send = self.aggregator.get_pending(participant)
        
        for pending_context in context_to_send:
            self.network_transport.send(Context(pending_context.people, pending_context.time_range, pending_context.attributes), participant, self.participants[participant])
        
        for context in context_to_send:
            self.pending[participant].remove(context)
                                       
        del self.participants[participant]

    def propagate_context(self, context):
        """
        Propagates context to all the registered clients
        
        :param context: message to be propagated
        """
        assert isinstance(context, Context)

        for participant in self.participants.keys():

            to_send = self.aggregator.aggregate(context, participant)

            if to_send:
                for message in to_send:
                    if not self.network_transport.send(message, participant, self.participants[participant]):
                        print 'Participant %s is probably dead. Removing it from the list' % participant
                        # del self.participants[participant]

    def send_context(self, context):
        """
        Propagates context to the clients specified in context.people
        
        :param context: message to be propagated
        """
        
        assert isinstance(context, Context)
        
        participants = {}
        for person in context.people:
            self.current_context[person] = context
            participants[person] = self.participants.get(person, [])
            if participants[person] == []:
                del participants[person]  
            
        for participant in participants.keys():

            to_send = self.aggregator.aggregate(context, participant)

            if to_send:
                for message in to_send:
                    if not self.network_transport.send(message, participant, self.participants[participant]):
                        print 'Participant %s is probably dead. Removing it from the list' % participant
                        # del self.participants[participant]
