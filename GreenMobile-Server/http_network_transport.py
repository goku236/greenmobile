"""
Http_network_transport
===============

Uses HTTP to send context to clients.

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

 """

import traceback
import urllib


__author__ = "Pedro.Alves, Filipe Barroso"

class Http_network_transport:
    
    def __init__(self, compress):
        self.comp = compress
    
    def send(self, context, recipient, recipient_info):
        """ Send Context to client 
        
        :param context: message to be propagated
        :param recipient: name of the client
        :param recipient_info: endpoint information
        """ 
        
        endpoint = recipient_info['endpoint']
 
        try:    
            handle = urllib.urlopen(endpoint, urllib.urlencode({'context': self.comp.compress(context)}))
             
            if handle.getcode() != 200: 
                raise Exception("Couldn't push message to %s: %s" % (recipient, handle.getcode()))
            else:
                print 'Propagated message to %s (%s)' % (recipient, endpoint)  
                return True
        except Exception, e:
            print "propagate: got an error trying to propagate Context to %s: %s" % (recipient, str(e))
            traceback.print_exc()
            return False

    def receive(self, context, recipient):
        pass

