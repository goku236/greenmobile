"""
Server
===============

GreenMobile server. Clients can register in it and then will be 
able to send and receive context.

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

The server can be configured by changing the file config.conf
 
"""

import json
import sys
import urllib

from bottle import route, run, request
import bottle
 
import compressor
from context_propagator import Context_propagator
from http_network_transport import Http_network_transport
import util


__author__ = "Pedro.Alves, Filipe Barroso"

context_propagator = None
comp = None
decomp = {}

def setup(aggregability, aggregation_functions, compression):
    """
    Server configuration
    
    
    :param aggregability: function that receives a context and a person and returns how many contexts 
        like those can be aggregated before being sent to this person
    :param aggregation_functions: functions that try to aggregate a previous context to a new context 
        into one and receive a maximum aggregation level
    :param compression: indicates if the compression module is on or off   
    """
    
    global context_propagator
    global comp
    
    
    comp = compression
            
    context_propagator = Context_propagator(Http_network_transport(comp), aggregability, aggregation_functions)
            
    print 'setup:received \n\taggregability:[%s]\n\taggregation_functions:[%s]\n' % (aggregability,
                                                                                     aggregation_functions)
@route('/register', method='POST')
def register():
    """ Register a client """
    
    global decomp
    
    if not context_propagator:
        raise Exception("Middleware not initialized. Call setup first.")

    participant = request.forms.get('participant')
    if not participant:
        return bottle.Response(status=500, body='Error - Missing participant parameter')

    port = request.forms.get('port')
    if not port:
        return bottle.Response(status=500, body='Error - Missing port parameter')
    
    decompres = request.forms.get('compression') 
    if not decompres: 
        return bottle.Response(status=500, body='Error - Missing compress parameter')
    
    decompressor_class = getattr(compressor, decompres)
    decomp[participant] = decompressor_class()
    
    callback_url = 'http://%s:%s/%s' % (request.remote_addr, port, participant)
 
    try:
        urllib.urlopen(callback_url)
    except Exception:
        print 'Couldn\'t ping %s' % callback_url
        bottle.abort(text='Provided callback endpoint (%s) is unreachable' % callback_url)

    context_propagator.register_participant(participant, {'endpoint': callback_url})

    print 'register:%s from %s OK' % (participant, request.remote_addr)

    return comp.__class__.__name__

@route('/participants', method='GET')
def participants(): 
    """ Returns to the client the information about all the users registered in the server """
    
    if not context_propagator:
        raise Exception("Middleware not initialized. Call setup first.")

    return context_propagator.participants

@route('/propagate', method='POST')
def propagate():
    """ multicast: propagates messages to all the clients """
    
    global decomp
    
    context = decomp[request.forms.get('participant')].decompress(request.forms.get('context'))
    
    if not context_propagator:
        raise Exception("Middleware not initialized. Call setup first.")
 
    return context_propagator.propagate_context(context)

@route('/send', method='POST')
def send():
    """ unicast: sends the message to the clients specified in context.people """

    context = decomp[json.loads(request.forms.get('participant'))].decompress(request.forms.get('context'))

    if not context_propagator:
        raise Exception("Middleware not initialized. Call setup first.")

    return context_propagator.send_context(context)

@route('/unregister', method='POST')
def unregister():
    """ Unregister a client """
    
    if not context_propagator:
        raise Exception("Middleware not initialized. Call setup first.")
    
    participant = request.forms.get('participant')
    if not participant:
        return bottle.Response(status=500, body='Error - Missing participant parameter')
    
    return context_propagator.unregister_participant(participant)

if __name__ == '__main__':
    bottle.debug(True)
    config = {}
    execfile("config.conf", config)
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    else:
        port = 8081
    setup(config["aggregability"], config["aggregation_functions"], config["compression"])        
    run(host=util.get_public_ip(), port=port, reloader=True, server='paste')
