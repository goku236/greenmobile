"""
TestApp
===============

TestApplication built on top of greenMobile taking advantage
of its capabilities to efficiently propagate messages to other
clients.

This application has 2 buttons:
- Send messages: Starts propagating messages to other clients
- Stop: stops the propagation of messages

The application also has 2 text boxes that show the context sent
and received by the server.

This application can be configured by changing the file config.conf
 
"""

import sys
import time
from xml.dom import minidom

import kivy
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

import context
import profiler
import remote


kivy.require('1.0.9')



__author__ = "Filipe Barroso"

class TestApp(App):
    
    def build(self):
        """ Starts the application """
        
        self.testscreen = TestScreen()
        return self.testscreen
    
    def on_stop(self):
        """ Executed before application is stopeed """
        
        self.testscreen.unregister()
        
class TestScreen(GridLayout):
    
    def __init__(self, **kwargs):
        """ Initiates instance of TestScreen """
        
        super(TestScreen, self).__init__(**kwargs)
        
        self.config = {}
        execfile("config.conf", self.config)
        
        if len(sys.argv) > 1:
            remote.SERVER_URL = sys.argv[1]
        else:
            remote.SERVER_URL = self.config["server_ip"]    
            
        self.middleware_engine = remote.Middleware(self.config["participant"], self.config["aggregability"], self.config["aggregation_functions"], self.config["compression"], callback=self.receive)
        
        self.cols = 2
        self.sendButton = Button(text='Send messages')
        self.sendButton.bind(on_press=self.send)
        self.add_widget(self.sendButton)
        self.stopButton = Button(text='Stop')
        self.stopButton.bind(on_press=self.stop)
        self.add_widget(self.stopButton)
        
        self.sendContext = TextInput(text='Context information sent:')
        self.add_widget(self.sendContext)
        
        self.receiveContext = TextInput(text='Context information received')
        self.add_widget(self.receiveContext)
        self.counter = self.config["number_times"]
        
        self.profiler = profiler.Profiler(self.config["compression"])
        
        
    
    def send(self, evt=None):
        
        xmldoc = minidom.parse(self.config["xml_data"])
        itemlist = xmldoc.getElementsByTagName('record') 
        
        self.profiler.add_time(time.time())
        
        while self.counter > 0:
            
            for s in itemlist:
                locations = s.getElementsByTagName('Location')[0].firstChild.nodeValue.split(', ')
                ctx = context.Context(self.config["people"], self.config["time_range"], {'T': [s.getElementsByTagName('Name')[0].firstChild.nodeValue], 'N': [s.getElementsByTagName('Number')[0].firstChild.nodeValue],
                                                                                         'L': [(locations[0], locations[1]), (locations[0], locations[1])]})
                time.sleep(self.config["period"])
                self.middleware_engine.propagate_context(ctx)
                self.profiler.add_sent_context(ctx)
            self.counter = self.counter - 1
         
        self.receiveContext.text = self.receiveContext.text + '\n\n---------------------\n'
        self.sendContext.text = self.sendContext.text + '\n\n---------------------\n'
            
    def receive(self, context):
        """ Receives context from server """
        
        self.profiler.add_time(time.time())
        self.profiler.add_received_context(context)
        print '<< %s' % context

    def stop(self, evt=None):
        """ Stops sending context to server """
        
        self.receiveContext.text = self.receiveContext.text + '\n\n Writing results\n'
        self.sendContext.text = self.sendContext.text + '\n\n Writing results \n'
        
        self.profiler.write_results("Test_results.txt")
        
        self.receiveContext.text = self.receiveContext.text + '\n\n---------------------\n'
        self.sendContext.text = self.sendContext.text + '\n\n---------------------\n'
        
    def unregister(self, evt=None):
        """ Unregisters the client and shuts-down the middleware """
        
        self.middleware_engine.stop()
            
if __name__ == '__main__':
    TestApp().run()
