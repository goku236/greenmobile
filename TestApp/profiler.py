"""
Profiler
===============

This class profiles the execution of the TestApp application

Calculates:
    - delays
    - average delay
    - max delay
    - time spent communicating

 """
 
import sys
 
__author__ = "Filipe Barroso"

class Profiler:
    
    times = []
    delays = []
    received_contexts = []
    sent_contexts = []
    
    def __init__(self, compression):
        self.cmp = compression
    
    def add_sent_context(self, context):
        self.sent_contexts.append(context)
    
    def add_time(self, number):
        """ 
        Adds a time value to the list 
        
        :param number: instant of time that is going to be added to the list
        """
        
        self.times.append(number)
        
        
    def add_received_context(self, context):
        self.received_contexts.append(context)    
        
    def calculate_delays(self):
        """Calculates the latency of each message received """
        
        previous_delay = self.times[0]    
        for delay in self.times[1:]:
            self.delays.append(delay - previous_delay)
            previous_delay = delay    
    
    def calculate_average_delays(self):
        """ Calculates the average of the delays """
        
        return sum(self.delays) / float(len(self.delays))
    
    def calculate_max_delays(self):
        """ Calculates the maximum value of the delays list """
        
        return max(self.delays)
    
    def calculate_size(self, contexts):
        total = 0
        
        for ctx in contexts:
            total += sys.getsizeof(self.cmp.compress(ctx))
        
        return total
    
    def calculate_total_time(self):
        """ Calcualtes the total time spent communicating """
        
        return self.times[-1] - self.times[0]
        
    def write_results(self, name_file):
        """ 
        Writes the results to a file
        
        :param name_file: name of the file
        """
        
        self.calculate_delays()
        with open(name_file, "w") as myfile:
            """
            for time_var in self.times:
                myfile.write('time: ' + str(time_var) + '\n')
            
            for delay in self.delays:
                myfile.write('delay: ' + str(delay) + '\n')
            """
            myfile.write('average delay: ' + str(self.calculate_average_delays()) + '\n')
            myfile.write('maximum delay: ' + str(self.calculate_max_delays()) + '\n')
            myfile.write('total time: ' + str(self.calculate_total_time()) + '\n')
            myfile.write('Download: ' + str(self.calculate_size(self.received_contexts)) + '\n')
            #myfile.write('Upload: ' + str(self.calculate_size(self.sent_contexts)) + '\n')
                