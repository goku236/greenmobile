"""
Remote
===============

Component responsible for performing the communication with the server.

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile
 
"""

from threading import Thread
import urllib

from bottle import Bottle, run, Route, request

import aggregator
import compressor
from context import Context
import util


__author__ = "Pedro.Alves, Filipe Barroso"

SERVER_URL = None
DEFAULT_PUSH_PORT = 8765
middleware_app = Bottle()
callback_thread = None 

def run_bottle(port):
    public_ip = util.get_public_ip()
    print 'Starting callback server at ip %s port %s' % (public_ip, port)
    run(middleware_app, server='paste', host=public_ip, port=port)

class Middleware():
    
    callbacks = dict()

    def __init__(self, participant, aggregability, aggregation_functions, compression, port=DEFAULT_PUSH_PORT, callback=None):
        """
        Initiates instance of Middleware
        
        Registers this participant into the server. The server will store the IP and callback port
        of the participant
        e.g., http://myip:7654/participant

        Each participant can only be associated with one ip:port. If the radiator receives
        a new ip:port for an existent participant, it will overwrite the previous ip:port
        
        :param participant: name of the client
        :param aggregability: function that receives a context and a person and returns how many contexts 
            like those can be aggregated before being sent to this person
        :param aggregation_functions: functions that try to aggregate a previous context to a new context 
            into one and receive a maximum aggregation level
        :param compression: indicates if the compression module is on or off
        :param port: client communication port
        :param callback: function that receives one argument (context). 
            Updates will be transmitted to the provided callback.
        """
        
        self.participant = participant
        self.aggregator = aggregator.Aggregator(aggregability, aggregation_functions) 
        
        
        self.comp = compression   
        self.callback_thread = Thread(target=run_bottle, args=(port,))
        self.callback_thread.daemon = True
        self.callback_thread.start() 

        route = Route(app=middleware_app, callback=self.receive, method='POST', rule='/%s' % self.participant)
        middleware_app.routes.append(route)
        middleware_app.router.add(route.rule, route.method, route, name=route.name)
        print 'Started endpoint: %s' % route.rule

        try:
            response = urllib.urlopen(SERVER_URL + '/register', urllib.urlencode({'participant': self.participant,
                                                                                  'port': port,
                                                                                  'compression': self.comp.__class__.__name__})) 
            if response.code != 200:
                print "register: got an error trying to register %s in %s. Server returned %s" % (self.participant, SERVER_URL, response.read())
            else:
                print 'Register OK'
            
            decompressor_class = getattr(compressor, response.read())
            self.decompressor = decompressor_class()
        except Exception, e:
            print "register: got an error trying to register %s in %s : %s" % (self.participant, SERVER_URL, str(e))

        if callback:
            self.callbacks[self.participant] = callback
    
    def propagate_context(self, context):
        """ 
        Propagates context to the server 
        
        :param: context: message
        """
        
        assert isinstance(context, Context)

        try:
            for message in self.aggregator.aggregate(context, self.participant):
                urllib.urlopen(SERVER_URL + '/propagate', urllib.urlencode({'context': self.comp.compress(message), 'participant': self.participant}))
        except Exception, e:
            print "propagate: got an error trying propagate to %s %s: %s" % (SERVER_URL, context, str(e))
            return False

        return True
    
    def send_context(self, context):
        """ 
        Propagates the context to the server. The server will the send the context to
            only the clients specified in context.people
            
        :param: context: message    
        """
        
        assert isinstance(context, Context)
        
        try:
            for message in self.aggregator.aggregate(context, self.participant):
                urllib.urlopen(SERVER_URL + '/send', urllib.urlencode({'context': self.comp.compress(message), 'participant': self.participant}))
           
        except Exception, e:
            print "unicast: got an error trying transmitt to %s %s: %s" % (SERVER_URL, context, str(e))
            return False

        return True

    
    def participants(self):
        """ Gets a list of the participants registered on the server and information about how to reach them """
        
        try:
            response = urllib.urlopen(SERVER_URL + '/participants')            
            participants_lst = response.read()
        except Exception, e:
            print "participants: got an error trying to get particiapnts to %s %s: %s" % (SERVER_URL, str(e))
            return False
        return participants_lst
    
    def receive(self):
        """ Receives context from server """
        
        context = self.decompressor.decompress(request.forms.get('context'))
        self.callbacks[request.path.strip('/')](context)

    def stop(self):
        """ Stops the middleware, that is, it closes the callback endpoint """
        
        global middleware_app
        
        """ Unregisters a participant. All pending messages will be propagated but new messages won't be"""
        
        try:
            urllib.urlopen(SERVER_URL + '/unregister', urllib.urlencode({'participant': self.participant})).read()
        except Exception, e:
            print("unregister: got an error trying to unregister %s from %s : %s" % (self.participant, SERVER_URL, str(e)))
            return False


        del self.callbacks[self.participant]

        middleware_app.close()
