"""
Aggregability
===============

Aggregability functions that can be used to configure the server and the clients

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

 """ 

__author__ = "Pedro.Alves, Filipe Barroso"

import util


def set_type(aggr_type, value):
    """ 
    Returns an annonymous function that returns aggregation_type: value 
    
    :param aggr_type: aggregation type. Can be 'volume', 'people' or 'time'
    :param value: value that can be associated with the aggregation type 
    """
    
    return lambda p, c: {aggr_type: value}

def priorities(pr_list):
    """
    Defines a functions that sets the priorities of each message depending on the content.
    The parameter p defines the priority of the message. If not defined is the same as 'p': 0
    
    
    :param pr_list: list of priorities associating an aggregability type with a value.
                    e.g. [{'volume':5}, {'volume':2}]
    """
    
    
    def auxiliar_func(current_participant_context, pending_context):
        if 'p' in current_participant_context.attributes:
            return pr_list[current_participant_context.attributes['p'][-1]]
        else:
            return pr_list[0]
        
    return auxiliar_func
    

def anonymize(aggr_type, value, lst_tags):
    """
    Function that aggregates messages that contains the specified tags
    
    :param aggr_type: aggregation type. Can be 'volume', 'people' or 'time'
    :param value: value that can be associated with the aggregation type
    :param lst_tags: list of tags that specify which messages to annonymize
    """
    
    def auxiliar_func(current_participant_context, pending_context):
        for tag in lst_tags:
            if tag in current_participant_context.attributes:
                return {aggr_type: value}
    
    return auxiliar_func

def proximity(current_participant_context, pending_context):
    """ Calculates the correlation based on the euclidean correlation between their location """

    if not current_participant_context:
        return {'volume': 1}

    person_last_location = current_participant_context.attributes['loc'][-1]
    avg_person_last_location = ((person_last_location[0][0] + person_last_location[1][0]) / 2,
                                (person_last_location[0][1] + person_last_location[1][1]) / 2)
    context_last_location = pending_context.attributes['loc'][-1]
    avg_context_last_location = ((context_last_location[0][0] + context_last_location[1][0]) / 2,
                                 (context_last_location[0][1] + context_last_location[1][1]) / 2)
    euclidean_distance = max(1, int(round(util.euclidean(avg_person_last_location, avg_context_last_location))))

    if euclidean_distance > 100:
        return {'volume': 50}
    else:
        return {'volume': int(round(euclidean_distance / 10))}