"""
Compressor
===============

Classes that perform the compression and decompression of messages.

To create a compression implementation the develloper must extend 
the abstract class Compressor and implement the methods compress and
decompress.

 """ 
 
import abc
import json

import context


__author__ = "Filipe Barroso"

class Compressor:
    """Abstract class. Compression algorithms implementations must extend Compressor"""
   
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def compress(self, context):
        """ 
        Receives context and returns a compressed string representing the original context
        
        :param context: message to be propagated
        """
        
        return
    
    @abc.abstractmethod
    def decompress(self, compressed_context):
        """ 
        Receives compressed context and decompresses it
        
        :param compressed_context: a compressed representation of context
        """
        
        return 
           
class Compressor_bz2(Compressor):
    """ 
    Compresses and decompresses using bzip2
    The library used is bz2 in: https://docs.python.org/2/library/bz2.html
    """
    
    def compress(self, context):
        import bz2
        return  bz2.compress(json.dumps(context.toJson()))
    
    def decompress(self, compressed_context):
        import bz2
        context_json = json.loads(bz2.decompress(compressed_context))
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])

class Compressor_gzip(Compressor):
    """ 
    Compresses and decompresses using zlib
    The library used was gzip in: https://docs.python.org/3/library/zlib.html
    """
    
    def compress(self, context):
        import zlib
        return zlib.compress(json.dumps(context.toJson()))
    
    def decompress(self, compressed_context):
        import zlib
        context_json = json.loads(zlib.decompress(compressed_context))
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])

class Compressor_deflate(Compressor):
    """ 
    Compresses and decompresses using deflate
    The library used was zlib in: https://docs.python.org/3/library/zlib.html
    """
    
    def compress(self, context):
        import zlib
        return zlib.compress(json.dumps(context.toJson()))[2:-4]
    
    def decompress(self, compressed_context):
        import zlib
        context_json = json.loads(zlib.decompress(compressed_context, -15))
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])
 
class Compressor_lzw(Compressor):
    """ 
    Compresses and decompresses using lzw
    he library used was lzw in: http://pythonhosted.org//lzw/
    """
    
    def compress(self, context):
        import lzw
        return b"".join(lzw.compress(json.dumps(context.toJson())))
    
    def decompress(self, compressed_context):
        import lzw
        context_json = json.loads(b"".join(lzw.decompress(compressed_context)))
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])

        
class Compressor_smaz(Compressor):
    """ 
    Compressor and decompresses using SMAZ - for small strings
    The library used was smaz in: https://pypi.python.org/pypi/smaz/1.0
    """
    
    def compress(self, context):
        import lib.smaz
        return lib.smaz.compress(json.dumps(context.toJson()))
    
    def decompress(self, compressed_context):
        import lib.smaz
        context_json = json.loads(lib.smaz.decompress(compressed_context))
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])
    
class Compressor_off(Compressor):
    """ Doesnt perform compression"""
    
    def compress(self, context):
        return json.dumps(context.toJson())
    
    def decompress(self, compressed_context):
        context_json = json.loads(compressed_context)
        return context.Context(set(context_json['people']), context_json['time_range'], context_json['attributes'])    
