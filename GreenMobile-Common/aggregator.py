"""
Aggregator
===============

Class that performs the aggregation of messages

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

 """ 

import json

from context import Context
import util

__author__ = "Pedro.Alves, Filipe Barroso"

class Aggregator():
    
    def __init__(self, aggregability, aggregation_functions=dict()):
        """
        Initates instance of Aggregation_on
        
        :param: participant: name of the client
        :param: aggregability: function that receives a context and a person and returns how 
            many contexts like those can be aggregated before being sent to this person
        :param aggregation_functions: functions that try to aggregate a previous context to a 
            new context into one and receive a maximum aggregation level
        """
        
        assert isinstance(aggregability or [], list)

        self.aggregability = aggregability
        self.aggregation_functions = aggregation_functions

        self.current_context = {}
        self.pending = {}
    
    def get_pending(self, participant):
        """ 
        Returns the pending context 
        
        :param: participant: name of the client
        """
        
        return self.pending[participant]
    
    def aggregate(self, context, participant):
        """
        Aggregates context
        
        :param context: message
        :param participant: name of the client
        """       

        assert isinstance(context, Context)

        for person in context.people:
            self.current_context[person] = context

        to_send = []
        
        current_participant_context = self.current_context[participant]

        self.pending[participant] = self.pending.get(participant, [])
        self.pending[participant].append(context)

        pending_by_aggregability = dict()
        for pending_context in self.pending[participant]:

            for aggr in self.aggregability:

                a = aggr(current_participant_context, pending_context)

                assert type(a) == dict

                a_str = json.dumps(a)

                pending_by_aggregability.setdefault(a_str, [])
                pending_by_aggregability[a_str].append(pending_context)

                trigger_propagation = False
                if 'volume' in a:
                    trigger_propagation = len(pending_by_aggregability[a_str]) >= a['volume']

                if 'people' in a:
                    trigger_propagation = len(util.people_range(pending_by_aggregability[a_str])) >= a['people']

                if 'time' in a:
                    trigger_propagation = util.time_range(pending_by_aggregability[a_str]) >= a['time']

                if trigger_propagation:
                    appended_people = set()
                    appended_time = None
                    appended_attributes = dict()

                    for pending_msg in pending_by_aggregability[a_str]:
                        appended_people = appended_people.union(pending_msg.people)
                        appended_time = util.sum_ranges(pending_msg.time_range, appended_time)
                        
                        for k, v in pending_msg.attributes.items():
                            if appended_attributes.get(k):
                                appended_attributes[k].extend(v)
                            else:
                                appended_attributes[k] = list(v)
                                
                    for attr in self.aggregation_functions.keys():
                        appended_attributes[attr] = self.aggregation_functions[attr](appended_attributes[attr])       
                        
                    to_send.append(Context(appended_people, appended_time, appended_attributes))

                    for c in pending_by_aggregability[a_str]:
                        self.pending[participant].remove(c)
                    del pending_by_aggregability[a_str]
                
        return to_send