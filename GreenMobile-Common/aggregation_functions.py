"""
Aggregation_functions
===============

Aggregation functions that can be used to configure the server and the clients

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile    

 """ 

from collections import Counter


__author__ = "Pedro.Alves, Filipe Barroso"

 
def no_repetitive(pending_values_set):
    """ Removes the repetitive members """ 
    
    return [ list(x) for x in set(tuple(x) for x in pending_values_set) ]   

def average(pending_values_set):
    """ Calculates the average value of a list of numbers """
    
    return [sum(pending_values_set, 0.0) / len(pending_values_set)]
 
def mode(pending_values_set):
    """ Calculates the mode value of a list of numbers """
    
    d = {}
    for elm in pending_values_set:
        try:
            d[elm] += 1
        except(KeyError):
            d[elm] = 1
    keys = d.keys()
    maximum = d[keys[0]]
    for key in keys[1:]:
        if d[key] > maximum:
            maximum = d[key]
    max_k = []      
    for key in keys:
        if d[key] == maximum:
            max_k.append(key),
           
    return max_k

def minimun(pending_values_set):
    """ Calculates the minimum value of a list """
    
    return [min(pending_values_set)]

def maximum(pending_values_set):
    """ Calculates the maximum value of a list """
    
    return [max(pending_values_set)]

def range_of_list(pending_values_set):
    """ Calculates the range of list """
    
    return [max(pending_values_set) - min(pending_values_set)]

def median(pending_values_set):
    """ Calculates the median value of a list of numbers """
    
    sorts = sorted(pending_values_set)
    length = len(sorts)
    if not length % 2:
        return [(sorts[length / 2] + sorts[length / 2 - 1]) / 2.0]
    return [sorts[length / 2]]

def number_occurrences(pending_values_set,):
    """ Calculates the number of occurrences in a list """
    
    result = Counter(pending_values_set)
    
    return result.most_common()

def calculate_velocity(pending_values_set):
    """ Receiving a list with multiple positions (x) and the time instances (seconds)
        calculates the velocity using the formula vD/vt """
    
    x_position_min, min_time = pending_values_set[0][0], pending_values_set[0][1]
    
    x_position_max = x_position_min
    max_time = min_time
    
    for location in pending_values_set[1:]:
        x_position_min = min(x_position_min, location[0])
        x_position_max = max(x_position_max, location[0])
        min_time = min(min_time, location[1])
        max_time = max(max_time, location[1])
    
    if (max_time - min_time) == 0:
        return [0]
    else:     
        return [(x_position_max - x_position_min) / (max_time - min_time)]

def calculate_velocity_xy(pending_values_set):
    """ Receiving a list with multiple positions (x, y) and the time instances (seconds)
        calculates the velocity using the formula vD/vt. It calculate vx and vy. """
    
    x_position_min, y_position_min, min_time = pending_values_set[0][0], pending_values_set[0][1], pending_values_set[0][2]
    
    x_position_max = x_position_min
    y_position_max = y_position_min
    max_time = min_time
    
    for location in pending_values_set[1:]:
        x_position_min = min(x_position_min, location[0])
        x_position_max = max(x_position_max, location[0])
        y_position_min = min(y_position_min, location[1])
        y_position_max = max(y_position_max, location[1])
        min_time = min(min_time, location[2])
        max_time = max(max_time, location[2])
        
    if (max_time - min_time) == 0:
        return [0]
    else:         
        return [(x_position_max - x_position_min) / (max_time - min_time), (y_position_max - y_position_min) / (max_time - min_time)]
    
def calculate_velocity_xyz(pending_values_set):
    """ Receiving a list with multiple positions (x, y, z) and the time instances (seconds)
        calculates the velocity using the formula vD/vt. It calculate vx, vy, and vz. """
    
    x_position_min, y_position_min, z_position_min, min_time = pending_values_set[0][0], pending_values_set[0][1], pending_values_set[0][2], pending_values_set[0][3]
    
    x_position_max = x_position_min
    y_position_max = y_position_min
    z_position_max = z_position_min
    max_time = min_time
    
    for location in pending_values_set[1:]:
        x_position_min = min(x_position_min, location[0])
        x_position_max = max(x_position_max, location[0])
        y_position_min = min(y_position_min, location[1])
        y_position_max = max(y_position_max, location[1])
        z_position_min = min(z_position_min, location[2])
        z_position_max = max(z_position_max, location[2])
        min_time = min(min_time, location[2])
        max_time = max(max_time, location[2])
        
    if (max_time - min_time) == 0:
        return [0]
    else:         
        return [(x_position_max - x_position_min) / (max_time - min_time), (y_position_max - y_position_min) / (max_time - min_time), (z_position_max - z_position_min) / (max_time - min_time)]    

def sum_values(pending_values_set):
    """ Calculates the sum value of a list of numbers """
    
    return [sum(pending_values_set)]

def count(pending_values_set):
    """ Calculates the number of members in the list """
    
    return [len(pending_values_set)]

def newest(pending_values_set):
    """ Returns the newest value """
     
    return pending_values_set[-1]

def minimum_bounding_box(pending_values_set):
    """Aggregates by finding the smallest square where all the locations can be found"""
        
    lowestX, lowestY, highestX, highestY = pending_values_set[0][0], pending_values_set[0][1], \
                                           pending_values_set[1][0], pending_values_set[1][1]
    for location in pending_values_set[1:]:
        lowestX = min(lowestX, location[0])
        lowestY = min(lowestY, location[1])
        highestX = max(highestX, location[0])
        highestY = max(highestY, location[1])
    
    return [(lowestX, lowestY), (highestX, highestY)]

def statistical_reduce(pending_values_set):
    """
    It reduces a list of values to the minimum size so that the statistical different between them remains the same
    Example: [T,F,F,T,T,T] -> [T,T,F]
    """

    attributes_list = list(pending_values_set)

    count = dict()
    for v in attributes_list:
        if v in count:
            count[v] += 1
        else:
            count[v] = 1

    if len(count) == 1:
        return count.keys()

    values = __reduce(count.values())

    if values == count.values():
        return attributes_list
    else:
        attributes_list = list()
        i = 0
        for k, v in count.items():
            attributes_list.append(k * values[i])

    return attributes_list

def __reduce(vals):

    for v in vals:
        if v % 2:
            return vals
        else:
            return __reduce([v / 2 for v in vals])

