"""
Util
===============

Utilitary methods.

Code taken from the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator
 
"""

import math
import socket
import sys


__author__ = 'Pedro.Alves'

def sum_ranges(range1, range2):
    """
    Calculates the union between two ranges.
    Example: sum_ranges(range(5,10),range(15,20)) = range(5,20)
    """

    if not range2:
        return range1

    lower_bound = min(range1[0], range2[0])
    upper_bound = max(range1[-1], range2[-1])

    return range(lower_bound, upper_bound + 1)


def euclidean(point1, point2):
    """
    Calculates the euclidean correlation between point1 and point2
    """

    sumSq = 0.0

    # add up the squared differences
    for i in range(len(point1)):
        sumSq += (point1[i] - point2[i]) ** 2

    # take the square root
    return sumSq ** 0.5

def entropy(string):
    """
    Calculates the Shannon entropy of a string
    Taken from http://splunk-base.splunk.com/answers/13636/calculate-entropy-just-entropy-not-change-in-entropy-like-associate
    """

    # get probability of chars in string
    prob = [ float(string.count(c)) / len(string) for c in dict.fromkeys(list(string)) ]

    # calculate the entropy
    entropy = -sum([ p * math.log(p) / math.log(2.0) for p in prob ])

    return entropy

def time_range_duration(time_range):
    """
    Calculates the duration of this range
    Assumes that range is chronologically sorted
    Example: range(5,10) -> 5
    """

    return time_range[-1] - time_range[0]


def time_range(context_list):
    """
    Calculates the difference between the oldest element in the list and newest element in the list
    """

    if not context_list:
        return 0

    _min, _max = sys.maxint, 0
    for c in context_list:
        _min = min(_min, c.time_range[0])
        _max = max(_max, c.time_range[-1])

    return _max - _min + 1

def people_range(context_list):
    """
    Returns a set with all the people involved in this list
    """
    
    appended_people = set()
    for msg in context_list:
        appended_people = appended_people.union(msg.people)

    return appended_people

def get_public_ip():
    """
    This function returns the public ip of localhost.
    As seen on http://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib#answer-166589
    """
    
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com", 80))
    public_ip = s.getsockname()[0]
    s.close()

    return public_ip
