"""
Context
===============

Represents a context to be propagated. A context is a set of attributes (e.g., location, activity)
that is associated with a set of people during a period of time

Code based on the project Radiator by Pedro Alves:
    https://bitbucket.org/anonymousJoe/radiator

Changed to meet the requirements of GreenMobile
 
"""

__author__ = "Pedro.Alves, Filipe Barroso"

class Context():
    
    def __init__(self, people, time_range, attributes):
        """
        Initates instance of Context
        
        :param people: set of people
        :param time_range: range
        :param attributes: dict (key=attribute_name, value=range)        
        """
        
        self.people = people
        self.time_range = time_range
        self.attributes = attributes

        assert isinstance(self.attributes, dict)
        for v in attributes.values():
            assert isinstance(v, list)
    
    def __repr__(self):
        """ To string method """
        
        return '%s | [%s,%s] | %s' % (self.people, self.time_range[0], self.time_range[-1], self.attributes)

    def __cmp__(self, other):
        """ Compares a context instance with another """
        
        if not other or not isinstance(other, Context):
            return 1

        if self.people != other.people:
            return 1

        if self.time_range != other.time_range:
            return 1

        if self.attributes != other.attributes:
            return 1

        return 0

    def toJson(self):
        """ Prepares an instance of Context to be dumped by json"""
        
        return { 'people' : list(self.people),
                 'time_range' : self.time_range,
                 'attributes' : self.attributes}